from api_request import _create_req_params
import argparser

def test_minumum_params():
    res = argparser.parse(['--date', '2018-03-30', '--from', 'BRQ', '--to', 'MUC'])
    p = _create_req_params(res)
    assert p['dateFrom'] == p['dateTo'] == '30/03/2018'
    assert p['flyFrom'] == 'BRQ'
    assert p['to'] == 'MUC'
    assert p['sort'] == 'price'


def test_fastest():
    res = argparser.parse(['--date', '2018-03-30', '--from', 'BRQ', '--to', 'MUC', '--fastest'])
    p = _create_req_params(res)
    assert p['dateFrom'] == p['dateTo'] == '30/03/2018'
    assert p['flyFrom'] == 'BRQ'
    assert p['to'] == 'MUC'
    assert p['sort'] == 'duration'


def test_return():
    res = argparser.parse(['--date', '2018-03-30', '--from', 'BRQ', '--to', 'MUC', '--return', '5'])
    p = _create_req_params(res)
    assert p['dateFrom'] == p['dateTo'] == '30/03/2018'
    assert p['flyFrom'] == 'BRQ'
    assert p['to'] == 'MUC'
    assert p['sort'] == 'price'
    assert p['daysInDestinationFrom'] == p['daysInDestinationTo'] == 5


def test_bags():
    res = argparser.parse(['--date', '2018-03-30', '--from', 'BRQ', '--to', 'MUC', '--bags', '3'])
    p = _create_req_params(res)
    assert p['dateFrom'] == p['dateTo'] == '30/03/2018'
    assert p['flyFrom'] == 'BRQ'
    assert p['to'] == 'MUC'
    assert p['sort'] == 'price'
