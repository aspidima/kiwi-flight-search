import argparser
from datetime import  datetime

def test_date():
    res = argparser.parse(['--date','2018-10-10'])
    assert res.date == datetime.strptime('2018-10-10', "%Y-%m-%d")


def test_from():
    res = argparser.parse(['--from','AAA'])
    assert res.dep == 'AAA'


def test_to():
    res = argparser.parse(['--to','AAA'])
    assert res.dest == 'AAA'


def test_one_way():
    res = argparser.parse(['--one-way'])
    assert res.one_way is True

def test_return():
    res = argparser.parse(['--return', '5'])
    assert res.days == 5

def test_bags():
    res = argparser.parse(['--bags','2'])
    assert res.bags == 2


def test_cheapest():
    res = argparser.parse(['--cheapest'])
    assert res.cheapest is True

def test_fastest():
    res = argparser.parse(['--fastest'])
    assert res.fastest is True
