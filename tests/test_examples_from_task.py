import logging
from book_flight import main
import argparser

def test1():
    args = argparser.parse('--date 2018-04-13 --from BCN --to DUB --one-way'.split(' '))
    res = main(args)
    logging.debug('pnr: "%s"', res)
    assert res is not None
    assert isinstance(res, str)


def test2():
    args = argparser.parse('--date 2018-04-13 --from LHR --to DXB --return 5'.split(' '))
    res = main(args)
    logging.debug('pnr: "%s"', res)
    assert res is not None
    assert isinstance(res, str)


def test3():
    args = argparser.parse('--date 2018-04-13 --from NRT --to SYD --cheapest --bags 2'.split(' '))
    res = main(args)
    logging.debug('pnr: "%s"', res)
    assert res is not None
    assert isinstance(res, str)


def test4():
    args = argparser.parse('--date 2018-04-13 --from CPH --to MIA --fastest'.split(' '))
    res = main(args)
    logging.debug('pnr: "%s"', res)
    assert res is not None
    assert isinstance(res, str)