import logging
import requests

PASSENGER = 'passenger.txt'
SKYPICKER_URL = 'https://api.skypicker.com/flights'
BOOKING_URL = 'http://128.199.48.38:8080/booking'

logger = logging.getLogger('API')

def _create_req_params(res):
    """Create request dictonary according to user's input.

    :return: dict with request parameters
    """
    params = {'flyFrom': res.dep,
              'to': res.dest,
              'dateFrom': res.date.strftime('%d/%m/%Y'),
              'dateTo': res.date.strftime('%d/%m/%Y')}
    # set number of days to return if --return was given
    if res.days:
        params.update({'daysInDestinationFrom': res.days,
                       'daysInDestinationTo': res.days})
    # set sorting by duration if --fastest was given, otherwise by price
    if res.fastest:
        params.update({'sort': 'duration'})
    else:
        params.update({'sort': 'price'})
    return params


def find_flight(args):
    """Make GET request to skypicker according to user input to find ticket.

    :param args: arguments received from user
    :type args: argparse.Namespace
    :returns: JSON data if request was successful, None otherwise
    """
    params = _create_req_params(args)
    logger.debug('Request params %s', params)
    resp = requests.get(SKYPICKER_URL, params=params)
    if resp.status_code != 200:
        return None
    # try to get JSON form response
    try:
        jdata = resp.json()
    except ValueError:
        return None

    return jdata


def _get_passenger():
    """Parses PASSENGER file.

    :return: dict of passenger's data
    """
    passenger = {}
    with open(PASSENGER, 'r') as f:
        for line in f.readlines():
            line = line.strip()
            line = line.split('=')
            passenger.update({line[0]: line[1]})
    return passenger


def book_flight(jdata, bags):
    """Creates and makes POST request to BOOKING_URL.

    :param jdata: flight data received from skypicker
    :param bags: number of bags to book
    :type jdata: JSON dict
    :type bags: int
    :returns: number of confirmed booking, None if booking was unsuccessful
    :rtype: str or None
    """
    headers = {"Content-Type": 'application/json'}
    passenger = _get_passenger()
    currency = jdata.get('currency', 'EUR')
    token = jdata.get('data')[0].get('booking_token')
    resp = requests.post(BOOKING_URL, headers=headers,
                         json={'currency': currency,
                               'passengers': passenger,
                               'booking_token': token,
                               'bags': bags})

    if resp.status_code != 200:
        return None

    jdata =  resp.json()
    if jdata['status'] != 'confirmed':
        return None

    return  jdata['pnr']
