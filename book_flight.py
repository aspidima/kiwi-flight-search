import sys
import logging
import argparser
import api_request

logging.basicConfig(level=logging.DEBUG)

MINIMUM = ['date', 'dep', 'dest']


def main(args):
    """Find flight, if found book ticket"""
    # check minimum amount of arguments
    for el in MINIMUM:
        if not args.__getattribute__(el):
            print('Too little arguments')
            return
    # send request to skypicker
    jdata = api_request.find_flight(args)
    # if request was unsuccessful
    if not jdata:
        return
    # if nothing found
    if not jdata.get('data'):
        return
    pnr = api_request.book_flight(jdata, args.bags)
    if pnr:
        return pnr
    return None



if __name__ == '__main__':
    args = argparser.parse(sys.argv[1:])
    logging.debug(args)
    conf_number =  main(args)
    if conf_number:
        print(conf_number)



