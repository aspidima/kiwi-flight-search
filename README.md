# Using
Clone code:

    git clone https://gitlab.com/aspidima/kiwi-flight-search.git
  Or download it [here](https://gitlab.com/aspidima/kiwi-flight-search/repository/master/archive.zip)
  and run:

     $ python3 book_flight.py --date 2018-04-13 --from NRT --to SYD --cheapest --bags 2
     $ CQGG7HA

   
# Get help

    python3 book_flight.py -h
    

# Parameters
|Parameter |Description  |
|--|--|
| --date **DATE** |  Date of flight YYYY-mm-dd|
| --from **DEP** | Departure airport IATA code, to check IATA codes visit [Wikipedia](https://en.wikipedia.org/wiki/IATA_airport_code#List) |
| --to **DEST** |  Destination airport IATA code|
| --bags **BAGS** |  Number of bags to book|
| --cheapest |  Find cheapest ticket. **Default**|
| --fastest |  Find fastest ticket|
| --one-way	|  No return ticket. **Default**|
| --return **DAYS** |  Number of nights to stay at destination|


# Conformation code
On successful booking you will get conformation code. Nothing otherwise

# Run tests

    pytest tests

