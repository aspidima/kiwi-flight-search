import argparse
from datetime import datetime
import csv
import os

curr_dir = os.path.dirname(os.path.abspath(__file__))
VALID_CODES_FILE = os.path.join(curr_dir, 'airports.dat')
# read file and create set of codes
with open(VALID_CODES_FILE, encoding='utf8') as f:
    reader = csv.reader(f, dialect='unix', delimiter=',')
    # IATA code is 4th in line
    VALID_CODES = {line[4] for line in reader if line[4] != r'\\N'}


def validate_date(date):
    """Validates date format and that date can't be before today.

    :param data: string date in "%Y-%m-%d" format
    :returns: given date or rises ArgumentTypeError
    """
    try:
        given_date = datetime.strptime(date, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{}'".format(date)
        raise argparse.ArgumentTypeError(msg)
    else:
        today = datetime.today()
        today_date = datetime(today.year, today.month, today.day)
        if given_date < today_date:
            msg = "Can't choose date before today"
            raise argparse.ArgumentTypeError(msg)
        return given_date


def validate_iata(code):
    """Validates IATA code.
    It can be API, database or file.
    For this example uses CSV file 'airports.dat' from openflights.

    :returns: given code or rise ArgumentTypeError if code isn't valid
    """
    if code not in VALID_CODES:
        msg = 'Not valid IATA code {}'.format(code)
        raise argparse.ArgumentTypeError(msg)
    return code


def parse(args=None):
    """Parses command line arguments.
    :return: argparse.Namespace of parsed arguments
    """
    parser = argparse.ArgumentParser(description="Book flight tickets")
    parser.add_argument('--date', type=validate_date,
                        help="Date of flight YYYY-mm-dd")
    parser.add_argument('--from', type=validate_iata, dest='dep',
                        help="Departure airport IATA code, "
                             "to check IATA codes visit "
                             "https://en.wikipedia.org/wiki/IATA_airport_code#List")
    parser.add_argument('--to', type=validate_iata, dest='dest',
                        help="Destination airport IATA code")
    parser.add_argument('--bags', type=int, default=1,
                        help="Number of bags to book")

    # tickets should be --cheapest or --fastest
    price_group = parser.add_mutually_exclusive_group()
    price_group.add_argument('--cheapest', action='store_true',
                             help="Find cheapest ticket. Default")
    price_group.add_argument('--fastest', action='store_true',
                             help="Find fastest ticket")


    # tickets should be --one-way or --return
    trip_group = parser.add_mutually_exclusive_group()
    trip_group.add_argument('--one-way',  default=True, action='store_true',
                            help="No return ticket. Default")
    trip_group.add_argument('--return', type=int, dest='days',
                            help="Number of nights to stay at destination")

    return parser.parse_args(args)